import 'package:lirik_lagu/lirik_lagu.dart' as lirik_lagu;
import 'dart:async';

void main() {
  final duration = Duration(seconds: 1);
  Timer.periodic(duration, (timer) {
    // Stop the timer when it matches a condition
    if (timer.tick >= 10) {
      timer.cancel();
    }

    print('Tick: ${timer.tick}');
  });
}
