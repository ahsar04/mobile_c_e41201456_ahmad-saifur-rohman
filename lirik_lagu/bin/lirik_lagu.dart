import 'package:lirik_lagu/lirik_lagu.dart' as lirik_lagu;
import 'dart:async';

void main() {
  print("Ready. Sing");

  // lirik lagu berupa data array
  var lirik = [
    'Hey manis, coba kau catat',
    'Keretaku tiba pukul empat sore',
    'Tak usah kau tanya aku ceritakan nanti',
    '-----------------------------',
    'Hey manis, kemana saja?',
    'Tak ada berita, sedikit cerita',
    'Tak kubaca lagi pesan di ujung malam',
    '-----------------------------',
    'Dan Jakarta muram kehilanganmu',
    'Terang lampu kota tak lagi sama',
    'Sudah saatnya kau tengok puing yang tertinggal',
    'Sampai kapan akan selalu berlari?',
    'Hingga kini masih selalu ku nanti-nanti',
    '-----------------------------',
    'Terbawa lagi langkahku ke sana',
    'Mantra apa entah yang istimewa',
    'Ku percaya selalu ada sesuatu di Jogja',
    'Dengar lagu lama ini katanya',
    'Izinkan aku pulang ke kotamu',
    'Ku percaya selalu ada sesuatu di Jogja',
    '-----------------------------',
    'Hey manis, bawa aku jalan',
    'Jalan kaki saja menyusuri kota',
    'Ceritakan semua ceritamu padaku',
    '-----------------------------',
    'Ya, Jakarta diam kehilanganmu',
    'Bau wangi hujan tak lagi sama',
    'Sudah saatnya kau jemput musik yang tertinggal',
    'Sampai kapan aku kan bernyanyi sendiri?',
    'Hingga kini masih selalu menanti-nanti',
    '-----------------------------',
    'Terbawa lagi langkahku ke sana',
    'Mantra apa entah yang istimewa',
    'Ku percaya selalu ada sesuatu di Jogja',
    'Dengar lagu lama ini katanya',
    'Izinkan aku pulang ke kotamu',
    'Ku percaya selalu ada sesuatu di Jogja',
    '-----------------------------',
    'Ingat waktu itu ku bertanya',
    'Aku mau dengar jawabnya',
    '-----------------------------',
    'Terbawa lagi langkahku ke sana',
    'Mantra apa entah yang istimewa',
    'Ku percaya selalu ada sesuatu di Jogja',
    'Dengar lagu lama ini katanya',
    'Izinkan aku pulang ke kotamu',
    'Ku percaya selalu ada sesuatu di Jogja',
    '-----------------------------',
  ];
  var timer = 3;// variabel untuk memberikan setiap melakukan perulangan
    for (var i = 0; i <= lirik.length-1; i++) {// melkukan perulangan sebanyak data array -1
      Timer(Duration(seconds: timer), () => print(lirik[i]));// print lirik lagu ke i+1 dengan durasi yang sudah ditentukan diawal
      timer += 6;//lirik berikutnya akan tampil pada detik ke timer+6
    }
}
