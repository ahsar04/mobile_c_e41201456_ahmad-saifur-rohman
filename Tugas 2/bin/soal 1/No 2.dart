
void main() {
  // variabel soal no 2
  var sentence = 'I am going to be Flutter Developer';
  var exampleFirstWord = sentence[0];
  var exampleSecondWord = sentence[2] + sentence[3];
  var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
  var fourthWord = sentence[11] + sentence[12];
  var fifthWord = sentence[14] + sentence[15];
  var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21] + sentence[22] + sentence[23];
  var seventhWord = sentence[25] + sentence[26] + sentence[27] + sentence[28] + sentence[29] + sentence[30] + sentence[31] + sentence[32] + sentence[33];
  
// output soal no 2
  print('Soal no 2');
  print('exampleFirstWord: '+exampleFirstWord);
  print('exampleSecondWord: '+exampleSecondWord);
  print('thirdWord: '+thirdWord);
  print('fourthWord: '+fourthWord);
  print('fifthWord: '+fifthWord);
  print('sixthWord: '+sixthWord);
  print('seventhWord: '+seventhWord);
}
