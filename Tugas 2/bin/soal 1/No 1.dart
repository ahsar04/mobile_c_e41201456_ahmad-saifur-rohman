import 'package:soal_1/soal_1.dart' as soal_1;

void main() {
  // variabel soal no 1
  var word = 'dart';
  var second = 'is';
  var third = 'awesome';
  var fourth = 'and';
  var fifth = 'i';
  var sixth = 'love';
  var seventh = 'it';

  // output soal no 1
  print('Soal no 1');
  print(word +
      ' ' +
      second +
      ' ' +
      third +
      ' ' +
      fourth +
      ' ' +
      fifth +
      ' ' +
      sixth +
      ' ' +
      seventh);
}
