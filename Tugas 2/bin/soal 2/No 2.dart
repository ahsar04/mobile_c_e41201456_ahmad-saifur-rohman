import 'dart:io';

String? nama;
String? peran;
void inputNama() {
  print('Masukan nama:');
  nama = stdin.readLineSync();
}

void cekNama() {
  if (nama == null || nama == '') {
    print('Nama harus di isi!');
    inputNama();
    return cekNama();
  } else {
    print('Halo $nama, pilih peranmu untuk memulai game!');
  }
}

void inputPeran() {
  print('Pilih peranmu (penyihir/guard/werewolf):');
  peran = stdin.readLineSync();
}

void cekPeran() {
  if (peran == null || peran == '') {
    inputPeran();
    return cekPeran();
  } else {
    if (peran == 'penyihir') {
    print('Selamat datang di Dunia Werewolf $nama');
      print(
          'Halo $peran $nama, kamu dapat melihat siapa yang menjadi werewolf!');
    } else if (peran == 'guard') {
    print('Selamat datang di Dunia Werewolf $nama');
      print(
          'Halo $peran $nama, kamu akan membantu temanmu dari serangan werewolf!');
    } else if (peran == 'werewolf') {
    print('Selamat datang di Dunia Werewolf $nama');
      print('Halo $peran $nama, kamu akan memakan mangsa setiap malam!');
    } else {
      print('Pilih sesuai petunjuk!');
      inputPeran();
      return cekPeran();
    }
  }
}


void main() {
  inputNama();
  cekNama();
  inputPeran();
  cekPeran();
}
