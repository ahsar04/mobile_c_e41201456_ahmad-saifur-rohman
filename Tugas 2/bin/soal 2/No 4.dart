var hari = 4;
var bulan = 5;
var tahun = 1945;
void switchCase() {
  switch (bulan) {
    case 1:
      {
        print("$hari Januari $tahun");
      }
      break;
    case 2:
      {
        print("$hari Februari $tahun");
      }
      break;
    case 3:
      {
        print("$hari Mart $tahun");
      }
      break;
    case 4:
      {
        print("$hari April $tahun");
      }
      break;
    case 5:
      {
        print("$hari Mei $tahun");
      }
      break;
    case 6:
      {
        print("$hari Juni $tahun");
      }
      break;
    case 7:
      {
        print("$hari Juli $tahun");
      }
      break;
    case 8:
      {
        print("$hari Agustus $tahun");
      }
      break;
    case 9:
      {
        print("$hari September $tahun");
      }
      break;
    case 10:
      {
        print("$hari Oktober $tahun");
      }
      break;
    case 11:
      {
        print("$hari November $tahun");
      }
      break;
    case 12:
      {
        print("$hari Desember $tahun");
      }
      break;
    default:
      {
        print("Invalid choice");
      }
      break;
  }
}

void main() {
  if (tahun >= 1900 && tahun <= 2200) {
    if (bulan >= 1 && bulan <= 12) {
      if (bulan % 2 == 0) {
        if (bulan == 2) {
          if (hari >= 1 && hari <= 28) {
            switchCase();
          } else {
            print("Hari harus dikisaran 1-28");
          }
        } else {
          if (hari >= 1 && hari <= 30) {
            switchCase();
          } else {
            print("Hari harus dikisaran 1-30");
          }
        }
      } else {
        if (hari >= 1 && hari <= 31) {
          switchCase();
        } else {
          print("Hari harus dikisaran 1-31");
        }
      }
    } else {
      print("Bulan harus dikisaran 1-12");
    }
  } else {
    print("Tahun harus dikisaran 1900-2200");
  }
}
