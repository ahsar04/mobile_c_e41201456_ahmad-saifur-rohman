import 'dart:io';

void main() {
  print('Apakah anda akan menginstall aplikasi? Y/T');
  String? answer = stdin.readLineSync();
  String output = answer == 'Y'
      ? 'Anda akan menginstall aplikasi dart'
      : answer == 'T'
          ? 'Aborted'
          : 'Y untuk melanjutkan, T untuk membatalkan';
  print(output);
}
