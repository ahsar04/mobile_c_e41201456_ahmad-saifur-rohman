class ChartModel {
  final String name;
  final String message;
  final String time;
  final String profileUrl;

  ChartModel(
      {required this.name,
      required this.message,
      required this.time,
      required this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'Tyo Mif',
      message: 'kontrakan',
      time: '19.08',
      profileUrl:
          'https://th.bing.com/th/id/OIP.8xpEsE8y5Hz3SpmWEanLGQHaHi?pid=ImgDet&rs=1'),
  ChartModel(
      name: 'Bayu TKK',
      message: 'Opo o le?',
      time: '15.26',
      profileUrl:
          'https://live.staticflickr.com/7027/6808994299_f66c96f4ba_b.jpg'),
  ChartModel(
      name: 'Hasan Tif',
      message: 'Info jumat berkah',
      time: '10.02',
      profileUrl:
          'https://th.bing.com/th/id/R.3a831d007134513706b041b5d10d1c0e?rik=zKCImjMewKGMvg&riu=http%3a%2f%2fpre07.deviantart.net%2f91a3%2fth%2fpre%2fi%2f2015%2f109%2fd%2f9%2fprofile_picture_by_aditthestig-d6tl1zj.jpg&ehk=poFXAxfLkQ3S6vfJEfpLlgIJ935exSZsnWeOOC6NB1U%3d&risl=&pid=ImgRaw&r=0'),
  ChartModel(
      name: 'Mas Tofa',
      message: 'sido',
      time: '08.01',
      profileUrl:
          'https://orig00.deviantart.net/91bb/f/2017/300/9/2/liamflawless_s_profile_picture_by_notkiler-dbrxtkm.png'),
  ChartModel(
      name: 'Nini-chan',
      message: 'ati-ati',
      time: 'yesterday',
      profileUrl:
          'https://th.bing.com/th/id/OIP.3p-rVOgZBq2m9htHnLKQPgHaHa?pid=ImgDet&w=600&h=600&rs=1'),
  ChartModel(
      name: 'SantriKoding',
      message: 'On-site mas',
      time: 'yesterday',
      profileUrl:
          'https://yt3.ggpht.com/a/AATXAJzRSGFjf6la0Uv2H2K3xfUZzjcWNALu33nclw=s900-c-k-c0xffffffff-no-rj-mo'),
  ChartModel(
      name: 'Kip 2020 Polije',
      message: 'Makasih',
      time: 'yesterday',
      profileUrl:
          'https://img00.deviantart.net/d0e4/i/2015/236/8/8/arxedus__logo_by_moerblx-d96zo0u.png'),
  ChartModel(
      name: 'Ardyas Mif',
      message: 'Nek onk casingku ',
      time: 'yesterday',
      profileUrl:
          'https://th.bing.com/th/id/OIP.J880ksn2dysE6PmLakt_mAHaHa?pid=ImgDet&rs=1'),
  ChartModel(
      name: 'Adam',
      message: 'Oke',
      time: 'yesterday',
      profileUrl:
          'https://th.bing.com/th/id/OIP.ME-9HpaQWGjvmyj0gNXDAQAAAA?pid=ImgDet&w=460&h=460&rs=1'),
];
